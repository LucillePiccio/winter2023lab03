import java.util.Scanner;
public class NationalPark{
	public static void main(String[]args){
		
		Cat[] clowderOfCats = new Cat[4];
		//filling in the array using a for loop
		for(int i = 0; i < clowderOfCats.length; i++){
			Scanner reader = new Scanner(System.in);
			Cat cat = new Cat();
			clowderOfCats[i] = cat; //cat1 added to clowerOfCats[i]
			
			System.out.println("Tell me about your cats color");
			String color = reader.nextLine();
			clowderOfCats[i].color = color;
			
			System.out.println("How old is the cat?");
			int age = Integer.parseInt(reader.nextLine());
			clowderOfCats[i].age = age;
			
			System.out.println("How about its personality? Is it affectionate, lazy or antisocial?");
			String personality = reader.nextLine();
			clowderOfCats[i].personality = personality;
		}
		//printing 3 fields of the last cat
		System.out.println("Here are the informations about the last cat");
		System.out.println("The cat is of color " + clowderOfCats[3].color);
		System.out.println("The cat is " + clowderOfCats[3].age + " years old.");
		System.out.println("The cat is " + clowderOfCats[3].personality);
		
		//calling 2 instance methods from cat.java of the first cat
		System.out.println("About the first cat:");
		clowderOfCats[0].action();
		clowderOfCats[0].sound();
	}
}