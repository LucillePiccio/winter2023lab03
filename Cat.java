public class Cat{
	public String color;
	public int age;
	public String personality;
	
	//method will take in the cat's age and will print its current action
	public void action(){
		if(this.age < 18){
			System.out.println("The cat is playing with its toys");
		}
		else if(this.age == 18){
			System.out.println("The cat is sitting on the couch and watching our tv show");
		}
		//when age > 18
		else{
			System.out.println("The cat is sleeping");
		}
	}
	//method takes in the cats personality and will print what sound it made
	public void sound(){
		if(this.personality.equals("lazy")){
			System.out.print("The cat went 'hiss'");
		}
		else if(this.personality.equals("affectionate")){
			System.out.print("The cat went 'meow'");
		}
		//when personality is antisocial
		else{
			System.out.println("The cat went 'purr'");
		}
	}
}